import {
  ClassSerializerInterceptor,
  HttpStatus,
  UnprocessableEntityException,
  ValidationError,
  ValidationPipe,
} from '@nestjs/common';
import { NestFactory, Reflector } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { useContainer } from 'class-validator';
import * as express from 'express';
import * as path from 'path';
import 'dotenv/config';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { cors: true });

  // Global prefix
  app.setGlobalPrefix('api');

  // Interceptors
  // app.useGlobalInterceptors(new ClassSerializerInterceptor(app.get(Reflector)));

  // Validations
  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      errorHttpStatusCode: HttpStatus.UNPROCESSABLE_ENTITY,
      validationError: {
        target: false,
      },
      exceptionFactory: (errors: ValidationError[]) => {
        const e = errors.map((error) => {
          const tmp = {};
          tmp[error.property] = [];
          const keys = Object.keys(error.constraints);
          keys.forEach((key) => {
            tmp[error.property].push(error.constraints[key]);
          });
          return tmp;
        });
        return new UnprocessableEntityException(e);
      },
    }),
  );

  // Container to use repositories in validators
  useContainer(app.select(AppModule), { fallbackOnErrors: true });

  // API Documentation
  if (process.env.NODE_ENV !== 'production') {
    const options = new DocumentBuilder()
      .setTitle('CHAMBER')
      .setDescription('CHAMBER Documentation')
      .setVersion('0.1')
      .addBearerAuth()
      .build();
    const document = SwaggerModule.createDocument(app, options);
    SwaggerModule.setup('documentation', app, document);
  }

  // JSON limits
  app.use(express.json({ limit: '50mb' }));
  app.use(express.urlencoded({ extended: true, limit: '50mb' }));

  app.use('/public', express.static(path.join(__dirname, '../public')));

  await app.listen(process.env.SERVER_PORT);
  console.info(
    `Welcome to Chamber. Server run on server http://127.0.0.1:${process.env.SERVER_PORT}/api`,
  );
}
bootstrap();
