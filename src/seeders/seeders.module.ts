import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { CategorySchema } from 'src/categories/schema/category.schema';
import { LanguajeSchema } from 'src/languajes/schema/languaje.schema';
import { TagSchema } from 'src/tags/schema/tag.schema';
import { LanguajesSeeder } from './languajes.seeder';
import { UserSchema } from 'src/user/schema/user.schema';
import { UserSeeder } from './user.seeder';
import { CategoryTagSeeder } from './categoriesTags.seeder';
import { ProfileSchema } from 'src/user/schema/profile.shema';
import { FileSchema } from 'src/files/schema/file.schema';
import { EventSchema } from 'src/events/schema/event.schema';
import { GroupSchema } from 'src/groups/schema/group.schema';
import { RollbackSeeder } from './rollback.seeder';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'user', schema: UserSchema },
      { name: 'profile', schema: ProfileSchema },
      { name: 'file', schema: FileSchema },
      { name: 'languaje', schema: LanguajeSchema },
      { name: 'category', schema: CategorySchema },
      { name: 'tag', schema: TagSchema },
      { name: 'event', schema: EventSchema },
      { name: 'group', schema: GroupSchema },
    ]),
  ],
  providers: [RollbackSeeder, UserSeeder, LanguajesSeeder, CategoryTagSeeder],
})
export class SeedersModule {}
