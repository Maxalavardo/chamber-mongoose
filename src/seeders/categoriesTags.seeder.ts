import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import * as fs from 'fs';
import { Seeder } from 'nestjs-seeder';
import { CategoryDocument } from 'src/categories/schema/category.schema';
import { TagDocument } from 'src/tags/schema/tag.schema';

@Injectable()
export class CategoryTagSeeder implements Seeder {
  constructor(
    @InjectModel('category')
    private readonly categoryModel: Model<CategoryDocument>,
    @InjectModel('tag')
    private readonly tagModel: Model<TagDocument>,
  ) {}
  async seed(): Promise<any> {
    const { categories } = JSON.parse(
      fs.readFileSync('src/json/categoriesTags.json', 'utf-8'),
    );

    try {
      for await (const category of categories) {
        const name = category.name;
        const categoryId = await this.categoryModel.create({ name });
        for await (const tag of category.tags) {
          await this.tagModel.create({ name: tag, category: categoryId._id });
        }
      }
    } catch (error) {
      console.log('Categories-Tags:', error.message);
    }
  }

  async drop(): Promise<any> {
    await this.categoryModel.deleteMany({});

    await this.tagModel.deleteMany({});

    return true;
  }
}
