import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Seeder } from 'nestjs-seeder';
import { UserDocument } from 'src/user/schema/user.schema';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UserSeeder implements Seeder {
  constructor(
    @InjectModel('user')
    private readonly userModel: Model<UserDocument>,
  ) {}

  async seed(): Promise<any> {
    try {
      const salt = await bcrypt.genSalt();
      await this.userModel.create({
        email: 'chamber@admin.com',
        password: await bcrypt.hash('1234567890', salt),
        confirmed_password: await bcrypt.hash('1234567890', salt),
        role: 'admin',
      });
    } catch (error) {
      console.log('Create User: ', error.message);
    }
  }

  async drop(): Promise<any> {
    await this.userModel.deleteMany({});

    return true;
  }
}
