import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Seeder } from 'nestjs-seeder';
import { CategoryDocument } from 'src/categories/schema/category.schema';
import { EventDocument } from 'src/events/schema/event.schema';
import { FileDocument } from 'src/files/schema/file.schema';
import { GroupDocument } from 'src/groups/schema/group.schema';
import { LanguajeDocument } from 'src/languajes/schema/languaje.schema';
import { TagDocument } from 'src/tags/schema/tag.schema';
import { ProfileDocument } from 'src/user/schema/profile.shema';
import { UserDocument } from 'src/user/schema/user.schema';
import { rmdir } from 'fs/promises';

@Injectable()
export class RollbackSeeder implements Seeder {
  constructor(
    @InjectModel('user')
    private readonly userModel: Model<UserDocument>,
    @InjectModel('profile')
    private readonly profileModel: Model<ProfileDocument>,
    @InjectModel('file')
    private readonly fileModel: Model<FileDocument>,
    @InjectModel('languaje')
    private readonly languajeModel: Model<LanguajeDocument>,
    @InjectModel('category')
    private readonly categoryModel: Model<CategoryDocument>,
    @InjectModel('tag')
    private readonly tagModel: Model<TagDocument>,
    @InjectModel('event')
    private readonly eventModel: Model<EventDocument>,
    @InjectModel('group')
    private readonly groupModel: Model<GroupDocument>,
  ) {}

  async seed(): Promise<any> {
    /*  */
  }

  async drop(): Promise<any> {
    await this.userModel.deleteMany({});
    await this.profileModel.deleteMany({});
    await this.fileModel.deleteMany({});
    await this.languajeModel.deleteMany({});
    await this.categoryModel.deleteMany({});
    await this.tagModel.deleteMany({});
    await this.eventModel.deleteMany({});
    await this.groupModel.deleteMany({});

    rmdir('public', { recursive: true })
      .then(() => {
        /*  */
      })
      .catch((err) => {
        console.error('Something wrong happened removing public folder', err);
      });

    return true;
  }
}
