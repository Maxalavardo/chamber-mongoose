import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import * as fs from 'fs';
import { Seeder } from 'nestjs-seeder';
import { LanguajeDocument } from 'src/languajes/schema/languaje.schema';

@Injectable()
export class LanguajesSeeder implements Seeder {
  constructor(
    @InjectModel('languaje')
    private readonly languajeModel: Model<LanguajeDocument>,
  ) {}

  async seed(): Promise<any> {
    const { languajes } = JSON.parse(
      fs.readFileSync('src/json/languajes.json', 'utf-8'),
    );
    try {
      // the name of the constant is name because the schema model requires it so
      for await (const name of languajes) {
        await this.languajeModel.create({ name });
      }
    } catch (exception: any) {
      console.log('Create languaje: ', exception.message);
    }
  }

  async drop(): Promise<any> {
    await this.languajeModel.deleteMany({});

    return true;
  }
}
