import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateExampleDto } from './dto/create-example.dto';
import { UpdateExampleDto } from './dto/update-example.dto';
import { Example, ExampleDocument } from './schema/example.schema';

@Injectable()
export class ExampleService {
  constructor(
    @InjectModel(Example.name)
    private readonly exampleModel: Model<ExampleDocument>,
  ) {}

  async create(createExampleDto: CreateExampleDto): Promise<ExampleDocument> {
    return await this.exampleModel.create(createExampleDto);
  }

  async findAll(): Promise<ExampleDocument[]> {
    return await this.exampleModel.find();
  }

  async findOne(id: string): Promise<ExampleDocument> {
    const example = await this.exampleModel.findById(id);

    if (!example) throw new NotFoundException();

    return example;
  }

  async update(
    id: string,
    updateExampleDto: UpdateExampleDto,
  ): Promise<ExampleDocument> {
    let update = await this.findOne(id);
    update = await this.exampleModel.findByIdAndUpdate(
      update._id,
      updateExampleDto,
      { new: true },
    );
    return update;
  }

  async remove(id: string): Promise<ExampleDocument> {
    const deleted = await this.findOne(id);
    return await this.exampleModel.findByIdAndRemove(deleted._id);
  }
}
