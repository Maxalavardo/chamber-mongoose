import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

// validation
export type ExampleDocument = Example & Document;

@Schema({ timestamps: true })
export class Example {
  @Prop()
  name: string;

  @Prop()
  number: number;

  @Prop()
  date: Date;

  // email required
  @Prop({ unique: true, required: true })
  email: string;

  @Prop({ type: Date })
  createdAt?: Date;

  @Prop({ type: Date })
  updatedAt?: Date;

  @Prop({ type: Date })
  deletedAt?: Date;
}

export const ExampleSchema = SchemaFactory.createForClass(Example);
