import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Res,
  HttpStatus,
} from '@nestjs/common';
import { ExampleService } from './example.service';
import { CreateExampleDto } from './dto/create-example.dto';
import { UpdateExampleDto } from './dto/update-example.dto';

@Controller('example')
export class ExampleController {
  constructor(private readonly exampleService: ExampleService) {}

  @Post()
  async create(@Body() createExampleDto: CreateExampleDto) {
    return await this.exampleService.create(createExampleDto);
  }

  @Get()
  async findAll() {
    return await this.exampleService.findAll();
  }

  @Get(':id')
  async findOne(@Param('id') id: string) {
    return await this.exampleService.findOne(id);
  }

  @Patch(':id')
  async update(
    @Param('id') id: string,
    @Body() updateExampleDto: UpdateExampleDto,
  ) {
    return await this.exampleService.update(id, updateExampleDto);
  }

  @Delete(':id')
  async remove(@Res() res: any, @Param('id') id: string) {
    const example = await this.exampleService.remove(id);
    return res.status(HttpStatus.OK).json({
      message: 'Example deleted succesfully',
      example,
    });
  }
}
