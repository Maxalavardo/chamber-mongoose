import { IsDateString, IsEmail, IsNumber, IsString } from 'class-validator';

export class CreateExampleDto {
  @IsString()
  name: string;

  @IsNumber()
  number: number;

  @IsDateString()
  date: Date;

  @IsEmail()
  email: string;
}
