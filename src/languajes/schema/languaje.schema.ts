import { Schema, SchemaFactory, Prop } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type LanguajeDocument = Languaje & Document;
@Schema({ timestamps: true })
export class Languaje {
  @Prop({ type: String, required: true, unique: true })
  name: string;
}

export const LanguajeSchema = SchemaFactory.createForClass(Languaje);
