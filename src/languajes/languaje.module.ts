import { Module } from '@nestjs/common';
import { LanguajeService } from './languaje.service';
import { LanguajeController } from './languaje.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { LanguajeSchema } from './schema/languaje.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'languaje', schema: LanguajeSchema }]),
  ],
  controllers: [LanguajeController],
  providers: [LanguajeService],
})
export class LanguajeModule {}
