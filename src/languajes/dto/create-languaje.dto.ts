import { IsString } from 'class-validator';

export class CreateLanguajeDto {
  @IsString()
  name: string;
}
