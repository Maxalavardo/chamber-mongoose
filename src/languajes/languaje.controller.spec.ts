import { Test, TestingModule } from '@nestjs/testing';
import { LanguajeController } from './languaje.controller';
import { LanguajeService } from './languaje.service';

describe('LanguajeController', () => {
  let controller: LanguajeController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [LanguajeController],
      providers: [LanguajeService],
    }).compile();

    controller = module.get<LanguajeController>(LanguajeController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
