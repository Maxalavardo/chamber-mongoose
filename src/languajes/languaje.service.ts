import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateLanguajeDto } from './dto/create-languaje.dto';
import { UpdateLanguajeDto } from './dto/update-languaje.dto';
import { LanguajeDocument } from './schema/languaje.schema';

@Injectable()
export class LanguajeService {
  constructor(
    @InjectModel('languaje')
    private readonly languajeModel: Model<LanguajeDocument>,
  ) {}

  async create(createLanguajeDto: CreateLanguajeDto) {
    return await this.languajeModel.create(createLanguajeDto);
  }

  async findAll() {
    return await this.languajeModel.find().sort({ _id: -1 });
  }

  async findOne(id: string) {
    return await this.languajeModel.findById(id);
  }

  async update(id: string, updateLanguajeDto: UpdateLanguajeDto) {
    let languaje = await this.findOne(id);

    languaje = await this.languajeModel.findOneAndUpdate(
      { _id: id },
      updateLanguajeDto,
      { new: true },
    );

    return languaje;
  }

  async remove(id: string) {
    await this.findOne(id);

    const languaje = await this.languajeModel.findOneAndDelete({ _id: id });

    return languaje;
  }
}
