import { Test, TestingModule } from '@nestjs/testing';
import { LanguajeService } from './languaje.service';

describe('LanguajeService', () => {
  let service: LanguajeService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [LanguajeService],
    }).compile();

    service = module.get<LanguajeService>(LanguajeService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
