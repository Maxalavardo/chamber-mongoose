import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
} from '@nestjs/common';
import { LanguajeService } from './languaje.service';
import { CreateLanguajeDto } from './dto/create-languaje.dto';
import { UpdateLanguajeDto } from './dto/update-languaje.dto';
import { Auth, AuthAll } from 'src/auth/auth.decorator';
import { RoleUserEnum } from 'src/user/enum/role-user.enum';

@Controller('languajes')
export class LanguajeController {
  constructor(private readonly languajeService: LanguajeService) {}

  @Auth(RoleUserEnum.ADMIN)
  @Post()
  create(@Body() createLanguajeDto: CreateLanguajeDto) {
    return this.languajeService.create(createLanguajeDto);
  }

  @AuthAll()
  @Get()
  findAll() {
    return this.languajeService.findAll();
  }

  @AuthAll()
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.languajeService.findOne(id);
  }

  @Auth(RoleUserEnum.ADMIN)
  @Put(':id')
  update(
    @Param('id') id: string,
    @Body() updateLanguajeDto: UpdateLanguajeDto,
  ) {
    return this.languajeService.update(id, updateLanguajeDto);
  }

  @Auth(RoleUserEnum.ADMIN)
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.languajeService.remove(id);
  }
}
