import { BadRequestException, Injectable } from '@nestjs/common';
import { transporter } from 'src/libs/mailerconfig';
import { UserService } from 'src/user/user.service';
import { ResetPasswordDto } from './dto/reset-password.dto';
import { ResetPassword } from './templates/reset-password.template';

@Injectable()
export class MailService {
  constructor(private readonly userService: UserService) {}

  async createEmail(resetPaword: ResetPasswordDto) {
    const verify = await this.userService.findUser(resetPaword.email);

    if (!verify) throw new BadRequestException('Email not found');

    try {
      await transporter.sendMail({
        from: `"Chamber APP" <${process.env.SMTP_USERNAME}>`, // sender address
        to: verify.email, // list of receivers
        subject: 'Request password change', // Subject line
        html: ResetPassword(resetPaword.link), // html body
      });
    } catch (error) {
      return error;
    }

    return { message: `Email send to ${verify.email}` };
  }
}
