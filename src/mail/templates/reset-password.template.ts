export const ResetPassword = (link: string) => {
  const mailer = `
        <h1>Password change request</h1>
        <p>Please click on the link below to change your password: ${link}</p>
    `;

  return mailer;
};
