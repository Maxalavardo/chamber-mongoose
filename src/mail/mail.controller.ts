import { Controller, Put, Body } from '@nestjs/common';
import { ResetPasswordDto } from './dto/reset-password.dto';
import { MailService } from './mail.service';

@Controller('mail')
export class MailController {
  constructor(private readonly mailService: MailService) {}

  @Put('reset-password')
  async create(@Body() resetPaword: ResetPasswordDto) {
    return await this.mailService.createEmail(resetPaword);
  }
}
