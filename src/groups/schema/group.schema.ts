import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { Category } from 'src/categories/schema/category.schema';
import { TypeEventGroup } from 'src/events/enum/type_event_group.enum';
import { Tag } from 'src/tags/schema/tag.schema';
import { GenderUserEnum } from 'src/user/enum/gender_user.enum';
import * as paginate from 'mongoose-paginate-v2';
import { File } from 'src/files/schema/file.schema';

export type GroupDocument = Group & Document;

@Schema({ timestamps: true })
export class Group {
  @Prop({ type: String, required: true })
  name: string;

  @Prop({ type: Types.ObjectId, ref: 'Category', required: true })
  theme: Category;

  @Prop({ type: String, enum: TypeEventGroup, required: true })
  type: TypeEventGroup;

  @Prop({ type: String, required: true })
  location: string;

  @Prop({ type: String, required: true })
  description: string;

  @Prop({ type: String, required: true })
  ubication: string;

  @Prop({ type: [{ type: Types.ObjectId, ref: 'Tag' }] })
  nationality: Tag[];

  @Prop({ type: String, enum: GenderUserEnum })
  gender: GenderUserEnum;

  @Prop({ type: Number })
  ageInitial: number;

  @Prop({ type: Number })
  ageFinal: number;

  @Prop({ type: Types.ObjectId, ref: 'File' })
  file: File;
}

export const GroupSchema = SchemaFactory.createForClass(Group);
GroupSchema.plugin(paginate);
