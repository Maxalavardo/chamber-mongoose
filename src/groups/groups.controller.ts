import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
  Query,
  UseInterceptors,
  UploadedFiles,
} from '@nestjs/common';
import { GroupsService } from './groups.service';
import { CreateGroupDto } from './dto/create-group.dto';
import { UpdateGroupDto } from './dto/update-group.dto';
import { AuthAll } from 'src/auth/auth.decorator';
import { Storage } from 'src/libs/multer';

@Controller('groups')
export class GroupsController {
  constructor(private readonly groupsService: GroupsService) {}

  @AuthAll()
  @UseInterceptors(Storage)
  @Post()
  async create(
    @Body() createGroupDto: CreateGroupDto,
    @UploadedFiles() file: Array<Express.Multer.File>,
  ) {
    return await this.groupsService.create(createGroupDto, file);
  }

  @AuthAll()
  @Get()
  async findAll(@Query() query, @Query('pagination') pagination: boolean) {
    const { page, limit } = query;
    return await this.groupsService.findAll(pagination, page, limit);
  }

  @AuthAll()
  @Get(':id')
  async findOne(@Param('id') id: string) {
    return await this.groupsService.findOne(id);
  }

  @AuthAll()
  @Put(':id')
  async update(
    @Param('id') id: string,
    @Body() updateGroupDto: UpdateGroupDto,
  ) {
    return await this.groupsService.update(id, updateGroupDto);
  }

  @AuthAll()
  @Delete(':id')
  async remove(@Param('id') id: string) {
    return await this.groupsService.remove(id);
  }
}
