import { PartialType } from '@nestjs/swagger';
import { ArrayUnique, IsArray, IsMongoId, IsOptional } from 'class-validator';
import { File } from 'src/files/schema/file.schema';
import { Tag } from 'src/tags/schema/tag.schema';
import { CreateGroupDto } from './create-group.dto';

export class UpdateGroupDto extends PartialType(CreateGroupDto) {
  @ArrayUnique()
  @IsArray()
  @IsOptional()
  removeNationality: Tag[];

  @IsMongoId()
  @IsOptional()
  removeFile: File;
}
