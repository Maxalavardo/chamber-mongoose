import { Transform } from 'class-transformer';
import {
  ArrayUnique,
  IsArray,
  IsEnum,
  IsInt,
  IsMongoId,
  IsOptional,
  IsString,
  Min,
} from 'class-validator';
import { Category } from 'src/categories/schema/category.schema';
import { TypeEventGroup } from 'src/events/enum/type_event_group.enum';
import { Tag } from 'src/tags/schema/tag.schema';
import { GenderUserEnum } from 'src/user/enum/gender_user.enum';

export class CreateGroupDto {
  @Transform(({ value }) => String(value), { toClassOnly: true })
  @IsString()
  name: string;

  @Transform(({ value }) => String(value), { toClassOnly: true })
  @IsMongoId()
  theme: Category;

  @Transform(({ value }) => String(value), { toClassOnly: true })
  @IsEnum(TypeEventGroup)
  type: TypeEventGroup;

  @Transform(({ value }) => String(value), { toClassOnly: true })
  @IsString()
  location: string;

  @Transform(({ value }) => String(value), { toClassOnly: true })
  @IsString()
  @IsOptional()
  description: string;

  @Transform(({ value }) => String(value), { toClassOnly: true })
  @IsString()
  ubication: string;

  @Transform(
    ({ value }) => (Array.isArray(value) == false ? Array(value) : value),
    {
      toClassOnly: true,
    },
  )
  @IsArray()
  @ArrayUnique()
  @IsOptional()
  nationality: Tag[];

  @Transform(({ value }) => String(value), { toClassOnly: true })
  @IsEnum(GenderUserEnum)
  @IsOptional()
  gender: GenderUserEnum;

  @Transform(({ value }) => Number(value), { toClassOnly: true })
  @IsInt()
  @Min(18)
  @IsOptional()
  ageInitial: number;

  @Transform(({ value }) => Number(value), { toClassOnly: true })
  @IsInt()
  @Min(18)
  @IsOptional()
  ageFinal: number;
}
