import {
  BadRequestException,
  ConflictException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { unlinkSync } from 'fs';
import { Model, PaginateModel } from 'mongoose';
import { CategoryDocument } from 'src/categories/schema/category.schema';
import { FileDocument } from 'src/files/schema/file.schema';
import { TagDocument } from 'src/tags/schema/tag.schema';
import { CreateGroupDto } from './dto/create-group.dto';
import { UpdateGroupDto } from './dto/update-group.dto';
import { GroupDocument } from './schema/group.schema';

@Injectable()
export class GroupsService {
  constructor(
    @InjectModel('group')
    private readonly groupModel: Model<GroupDocument>,
    @InjectModel('group')
    private readonly groupModelPag: PaginateModel<GroupDocument>,
    @InjectModel('category')
    private readonly categoryModel: Model<CategoryDocument>,
    @InjectModel('tag')
    private readonly tagModel: Model<TagDocument>,
    @InjectModel('file')
    private readonly fileModel: Model<FileDocument>,
  ) {}

  async create(
    createGroupDto: CreateGroupDto,
    file?: Array<Express.Multer.File>,
  ) {
    let event = null,
      img = null;

    try {
      if (createGroupDto.ageInitial && !createGroupDto.ageFinal) {
        throw new BadRequestException('final age needed');
      } else if (!createGroupDto.ageInitial && createGroupDto.ageFinal) {
        throw new BadRequestException('initial age needed');
      }

      const theme = await this.categoryModel.findById(createGroupDto.theme);

      if (!theme) throw new BadRequestException('Theme not found');

      if (file.length == 1) {
        img = new this.fileModel();
        img.route = file[0].path;
        await img.save();
        img = img._id;
      }

      event = await this.groupModel.create({
        name: createGroupDto.name,
        theme: theme._id,
        type: createGroupDto.type,
        location: createGroupDto.location,
        description: createGroupDto.description,
        ubication: createGroupDto.ubication,
        nationality: createGroupDto.nationality,
        gender: createGroupDto.gender,
        ageInitial: createGroupDto.ageInitial,
        ageFinal: createGroupDto.ageFinal,
        file: img,
      });
    } catch (error) {
      return error;
    }

    return await this.groupModel
      .findById(event._id)
      .populate({
        path: 'theme',
        model: this.categoryModel,
      })
      .populate({
        path: 'nationality',
        model: this.tagModel,
        populate: {
          path: 'category',
          model: this.categoryModel,
        },
      })
      .populate({
        path: 'file',
        model: this.fileModel,
      });
  }

  async findAll(pagination?: boolean, page?: number, limit?: number) {
    if (pagination) {
      return await this.groupModelPag.paginate(
        {},
        {
          page: page ? page : 1,
          limit: limit ? limit : 10,
          populate: [
            {
              path: 'theme',
              model: this.categoryModel,
            },
            {
              path: 'nationality',
              model: this.tagModel,
              populate: {
                path: 'category',
                model: this.categoryModel,
              },
            },
            {
              path: 'file',
              model: this.fileModel,
            },
          ],
          sort: { _id: -1 },
        },
      );
    }

    return await this.groupModel
      .find()
      .sort({ _id: -1 })
      .populate({
        path: 'theme',
        model: this.categoryModel,
      })
      .populate({
        path: 'nationality',
        model: this.tagModel,
        populate: {
          path: 'category',
          model: this.categoryModel,
        },
      })
      .populate({
        path: 'file',
        model: this.fileModel,
      });
  }

  async findOne(id: string) {
    const group = await this.groupModel
      .findById(id)
      .populate({
        path: 'theme',
        model: this.categoryModel,
      })
      .populate({
        path: 'nationality',
        model: this.tagModel,
        populate: {
          path: 'category',
          model: this.categoryModel,
        },
      })
      .populate({
        path: 'file',
        model: this.fileModel,
      });

    if (!group) throw new NotFoundException('Group not fount');

    return group;
  }

  async update(id: string, updateGroupDto: UpdateGroupDto) {
    const updateGroup = await this.findOne(id);

    // addToSet and pullAll methods cannot coexist in the same update method
    await updateGroup.updateOne(
      {
        name: updateGroupDto.name,
        theme: updateGroupDto.theme,
        type: updateGroupDto.type,
        location: updateGroupDto.location,
        description: updateGroupDto.description,
        ubication: updateGroupDto.ubication,
        gender: updateGroupDto.gender,
        ageInitial: updateGroupDto.ageInitial,
        ageFinal: updateGroupDto.ageFinal,
        $addToSet: { nationality: { $each: updateGroupDto.nationality } },
      },
      { new: true },
    );

    if (updateGroupDto.removeFile) {
      const img = await this.fileModel.findById(updateGroupDto.removeFile);

      if (!img) throw new ConflictException('Image not fount');

      unlinkSync(img.route);
      await img.deleteOne();

      await updateGroup.updateOne({
        $unset: { file: updateGroupDto.removeFile },
      });
    }

    await updateGroup.updateOne(
      {
        $pullAll: {
          nationality: updateGroupDto.removeNationality,
        },
      },
      { new: true },
    );

    return await this.findOne(id);
  }

  async remove(id: string) {
    const group = await this.findOne(id);

    unlinkSync(group.file.route);
    await this.fileModel.findOneAndRemove({ route: group.file.route });

    return await this.groupModel
      .findByIdAndDelete(id)
      .populate({
        path: 'theme',
        model: this.categoryModel,
      })
      .populate({
        path: 'nationality',
        model: this.tagModel,
        populate: {
          path: 'category',
          model: this.categoryModel,
        },
      })
      .populate({
        path: 'file',
        model: this.fileModel,
      });
  }
}
