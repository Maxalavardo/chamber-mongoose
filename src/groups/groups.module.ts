import { Module } from '@nestjs/common';
import { GroupsService } from './groups.service';
import { GroupsController } from './groups.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { GroupSchema } from './schema/group.schema';
import { CategorySchema } from 'src/categories/schema/category.schema';
import { TagSchema } from 'src/tags/schema/tag.schema';
import { FileSchema } from 'src/files/schema/file.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'group', schema: GroupSchema },
      { name: 'category', schema: CategorySchema },
      { name: 'tag', schema: TagSchema },
      { name: 'file', schema: FileSchema },
    ]),
  ],
  controllers: [GroupsController],
  providers: [GroupsService],
})
export class GroupsModule {}
