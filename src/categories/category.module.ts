import { Module } from '@nestjs/common';
import { CategoryService } from './category.service';
import { CategoryController } from './category.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { CategorySchema } from './schema/category.schema';
import { TagSchema } from 'src/tags/schema/tag.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: 'category',
        schema: CategorySchema,
      },
      {
        name: 'tag',
        schema: TagSchema,
      },
    ]),
  ],
  controllers: [CategoryController],
  providers: [CategoryService],
})
export class CategoryModule {}
