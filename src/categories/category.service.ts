import {
  ConflictException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, PaginateModel } from 'mongoose';
import { TagDocument } from 'src/tags/schema/tag.schema';
import { CreateCategoryDto } from './dto/create-category.dto';
import { UpdateCategoryDto } from './dto/update-category.dto';
import { CategoryDocument } from './schema/category.schema';

@Injectable()
export class CategoryService {
  constructor(
    @InjectModel('category')
    private readonly categoryModel: Model<CategoryDocument>,
    @InjectModel('category')
    private readonly categoryModelPag: PaginateModel<CategoryDocument>,
    @InjectModel('tag')
    private readonly tagModel: Model<TagDocument>,
  ) {}

  async create(createCategoryDto: CreateCategoryDto) {
    if (createCategoryDto.color) {
      const color = await this.categoryModel.findOne({
        color: createCategoryDto.color,
      });

      if (color) throw new ConflictException('Color in use');
    }

    return await this.categoryModel.create(createCategoryDto);
  }

  async findAll(pagination?: boolean, page?: number, limit?: number) {
    if (pagination) {
      return await this.categoryModelPag.paginate(
        {},
        {
          page: page ? page : 1,
          limit: limit ? limit : 10,
          sort: { _id: -1 },
        },
      );
    }
    return await this.categoryModel.find().sort({ _id: -1 });
  }

  async findOne(id: string) {
    const category = await this.categoryModel.findById(id);

    if (!category) throw new NotFoundException('Category not found');

    return category;
  }

  async update(id: string, updateCategoryDto: UpdateCategoryDto) {
    let category = await this.findOne(id);

    category = await this.categoryModel.findOneAndUpdate(
      { _id: id },
      updateCategoryDto,
      { new: true },
    );

    return category;
  }

  async remove(id: string) {
    const category = await this.findOne(id);

    const tags = await this.tagModel.find({
      category: category._id,
    });

    for await (const remove of tags) {
      await this.tagModel.deleteOne({ remove });
    }

    return await this.categoryModel.findOneAndDelete({ _id: id });
  }
}
