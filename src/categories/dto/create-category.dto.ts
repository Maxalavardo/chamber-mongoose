import { IsHexColor, IsOptional, IsString } from 'class-validator';

export class CreateCategoryDto {
  @IsString()
  name: string;

  @IsHexColor()
  @IsOptional()
  color: string;
}
