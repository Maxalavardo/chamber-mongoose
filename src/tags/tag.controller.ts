import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
  Query,
} from '@nestjs/common';
import { TagService } from './tag.service';
import { CreateTagDto } from './dto/create-tag.dto';
import { UpdateTagDto } from './dto/update-tag.dto';
import { Auth, AuthAll } from 'src/auth/auth.decorator';
import { RoleUserEnum } from 'src/user/enum/role-user.enum';

@Controller('tags')
export class TagController {
  constructor(private readonly tagService: TagService) {}

  @Auth(RoleUserEnum.ADMIN)
  @Post()
  async create(@Body() createTagDto: CreateTagDto) {
    return await this.tagService.create(createTagDto);
  }

  @AuthAll()
  @Get()
  async findAll(@Query() query, @Query('pagination') pagination: boolean) {
    const { category_id, name, description, page, limit } = query;
    return await this.tagService.findAll(
      pagination,
      category_id,
      name,
      description,
      page,
      limit,
    );
  }

  @AuthAll()
  @Get(':id')
  async findOne(@Param('id') id: string) {
    return await this.tagService.findOne(id);
  }

  @Auth(RoleUserEnum.ADMIN)
  @Put(':id')
  async update(@Param('id') id: string, @Body() updateTagDto: UpdateTagDto) {
    return await this.tagService.update(id, updateTagDto);
  }

  @Auth(RoleUserEnum.ADMIN)
  @Delete(':id')
  async remove(@Param('id') id: string) {
    return await this.tagService.remove(id);
  }
}
