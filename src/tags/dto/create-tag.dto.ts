import { IsOptional, IsString } from 'class-validator';
import { Category } from 'src/categories/schema/category.schema';

export class CreateTagDto {
  @IsString()
  name: string;

  @IsString()
  @IsOptional()
  description: string;

  @IsString()
  category: Category;
}
