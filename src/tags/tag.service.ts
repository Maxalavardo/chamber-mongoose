import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, PaginateModel, Types } from 'mongoose';
import { CategoryDocument } from 'src/categories/schema/category.schema';
import { CreateTagDto } from './dto/create-tag.dto';
import { UpdateTagDto } from './dto/update-tag.dto';
import { TagDocument } from './schema/tag.schema';

@Injectable()
export class TagService {
  constructor(
    @InjectModel('tag')
    private readonly tagModel: Model<TagDocument>,
    @InjectModel('tag')
    private readonly tagModelPag: PaginateModel<TagDocument>,
    @InjectModel('category')
    private readonly categoryModel: Model<CategoryDocument>,
  ) {}

  async create(createTagDto: CreateTagDto) {
    const category = await this.categoryModel.findById(createTagDto.category);

    if (!category) throw new NotFoundException('Category not found');

    createTagDto.category = category._id;

    return await (
      await this.tagModel.create(createTagDto)
    ).populate({ path: 'category', model: this.categoryModel });
  }

  async findAll(
    pagination?: boolean,
    category_id?: string,
    name?: string,
    description?: string,
    page?: number,
    limit?: number,
  ) {
    const filter = new Object();

    if (category_id) {
      filter['category'] = new Types.ObjectId(category_id);
    }

    if (name) {
      filter['name'] = { $regex: name };
    }

    if (description) {
      filter['description'] = { $regex: description };
    }

    if (pagination) {
      return await this.tagModelPag.paginate(filter, {
        page: page ? page : 1,
        limit: limit ? limit : 10,
        populate: { path: 'category', model: this.categoryModel },
        sort: { _id: -1 },
      });
    } else {
      return await this.tagModel
        .find(filter)
        .sort({ _id: -1 })
        .populate({ path: 'category', model: this.categoryModel });
    }
  }

  async findOne(id: string) {
    const tag = await this.tagModel
      .findById(id)
      .populate({ path: 'category', model: this.categoryModel });

    if (!tag) throw new NotFoundException('Tag not found');

    return tag;
  }

  async update(id: string, updateTagDto: UpdateTagDto) {
    await this.findOne(id);

    return await this.tagModel
      .findByIdAndUpdate(id, updateTagDto, {
        new: true,
      })
      .populate({ path: 'category', model: this.categoryModel });
  }

  async remove(id: string) {
    await this.findOne(id);

    return await this.tagModel
      .findByIdAndDelete(id)
      .populate({ path: 'category', model: this.categoryModel });
  }
}
