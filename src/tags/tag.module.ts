import { Module } from '@nestjs/common';
import { TagService } from './tag.service';
import { TagController } from './tag.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { TagSchema } from './schema/tag.schema';
import { CategorySchema } from 'src/categories/schema/category.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'tag', schema: TagSchema },
      { name: 'category', schema: CategorySchema },
    ]),
  ],
  controllers: [TagController],
  providers: [TagService],
})
export class TagModule {}
