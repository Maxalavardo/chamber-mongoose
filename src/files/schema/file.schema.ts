import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type FileDocument = File & Document;

@Schema({
  timestamps: true,
  toJSON: {
    virtuals: true,
  },
})
export class File {
  @Prop({ type: String })
  route: string;
}

const FileSchema = SchemaFactory.createForClass(File);
FileSchema.virtual('fullPath').get(function (this: FileDocument) {
  return `${process.env.BASE_URL}/${this.route}`;
});

export { FileSchema };
