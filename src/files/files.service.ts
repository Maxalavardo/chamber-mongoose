import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { UpdateFileDto } from './dto/update-file.dto';
import { FileDocument } from './schema/file.schema';

@Injectable()
export class FilesService {
  constructor(
    @InjectModel('file')
    private readonly fileModel: Model<FileDocument>,
  ) {}

  async create(images: Array<Express.Multer.File>) {
    let save = null;
    const array = [];

    for await (const image of images) {
      save = new this.fileModel();
      save.route = image.path;
      await save.save();
      array.push(save);
    }

    return array;
  }

  async findAll() {
    return await `This action returns all files`;
  }

  findOne(id: number) {
    return `This action returns a #${id} file`;
  }

  update(id: number, updateFileDto: UpdateFileDto) {
    return `This action updates a #${id} file`;
  }

  remove(id: number) {
    return `This action removes a #${id} file`;
  }
}
