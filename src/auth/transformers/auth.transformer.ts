import { UserDocument } from 'src/user/schema/user.schema';
import { UserTransformer } from 'src/user/transformers/user.transformer';
import { IToken } from '../interfaces/itoken.interfaces';

export class AuthTransformer {
  public static transform(token: IToken) {
    const user: UserDocument = token.getUser();

    return {
      user: UserTransformer.transform(user),
      token: token.getHash(),
    };
  }
}
