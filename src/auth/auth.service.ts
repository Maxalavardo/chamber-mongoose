import {
  BadRequestException,
  ConflictException,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserService } from 'src/user/user.service';
import * as bcrypt from 'bcrypt';
import { JWTToken } from './jwt.token';
import { UserDocument } from 'src/user/schema/user.schema';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { ProfileDocument } from 'src/user/schema/profile.shema';

@Injectable()
export class AuthService {
  constructor(
    @InjectModel('user')
    private readonly userModel: Model<UserDocument>,
    @InjectModel('profile')
    private readonly profileModel: Model<ProfileDocument>,
    private readonly userService: UserService,
    private readonly jwtService: JwtService,
  ) {}

  async validateUser(email: string, pass: string): Promise<any> {
    const user = await this.userService.findUser(email);

    if (user && (await bcrypt.compare(pass, user.password))) {
      const { password, ...result } = user;
      return result;
    }

    return null;
  }

  async login(user: any): Promise<any> {
    const resp = new JWTToken(this.jwtService, user._doc);
    return resp;
  }

  async profile(user: UserDocument): Promise<UserDocument> {
    return await this.userService.findProfileUser(user);
  }

  async googleLogin(req: any) {
    if (!req.user)
      throw new UnauthorizedException('google credentials are incorrect');

    let user = await this.userModel.findOne({ googleID: req.user.id });

    if (user) return await this.login(user);

    user = await this.userModel.findOne({ email: req.user.email });

    if (user)
      throw new ConflictException(
        `User already exists, but Google account was not connected to user's account`,
      );

    try {
      const random = Math.floor(Math.random() * (100000000000 - 1 + 1) + 1);
      const hash = await this.userService.setHashPassword(
        this.userService.generateRandomText(),
      );

      const newUser = await this.userModel.create({
        email: req.user.email,
        password: hash,
        confirmed_password: hash,
        googleID: req.user.id,
        googleStatus: false,
      });

      await this.profileModel.create({
        username: `${req.user.firstName}${req.user.lastName}${random}`,
        first_name: req.user.firstName,
        last_name: req.user.lastName,
        birthdate: new Date(),
        user: newUser._id,
      });

      user = await this.login(newUser);
    } catch (error) {
      throw new BadRequestException(error);
    }

    return user;
  }
}
