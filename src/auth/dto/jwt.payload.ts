export class JWTPayload {
  _id: string;
  email: string;
  createdAt: Date;
}
