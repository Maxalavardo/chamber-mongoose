import { JwtService } from '@nestjs/jwt';
import { UserDocument } from 'src/user/schema/user.schema';
import { JWTPayload } from './dto/jwt.payload';
import { IToken } from './interfaces/itoken.interfaces';

export class JWTToken implements IToken {
  private readonly hash: string;
  private readonly user: any;
  private readonly payload: JWTPayload;

  constructor(private readonly jwtService: JwtService, user: UserDocument) {
    this.user = user;
    this.payload = {
      _id: user._id,
      email: user.email,
      createdAt: user.createdAt,
    };

    this.hash = this.jwtService.sign(this.payload);
  }

  getHash(): string {
    return this.hash;
  }

  getPayload(): JWTPayload {
    return this.payload;
  }

  getUser(): any {
    return this.user;
  }
}
