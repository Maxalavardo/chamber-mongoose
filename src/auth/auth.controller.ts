import {
  Controller,
  Get,
  Post,
  UseGuards,
  UseInterceptors,
  Res,
  Req,
} from '@nestjs/common';
import { ApiOperation } from '@nestjs/swagger';
import { Request, Response } from 'express';
import { AuthAll } from './auth.decorator';
import { AuthService } from './auth.service';
import { LocalAuthGuard } from './guards/local-auth.guard';
import { LoginInterceptor } from './transformers/auth.interceptor';
// import { GoogleAuthGuard } from './guards/google.guard';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @UseGuards(LocalAuthGuard)
  @UseInterceptors(LoginInterceptor)
  @Post('login')
  async login(@Req() req) {
    return await this.authService.login(req.user);
  }

  @AuthAll()
  @Post('logout')
  @ApiOperation({ summary: 'Logout' })
  singOut(@Res() res: Response) {
    res.send({ statusCode: res.statusCode, data: { token: '' } });
  }

  @AuthAll()
  @Get('profile')
  async profile(@Req() req) {
    return await this.authService.profile(req.user);
  }

  @UseInterceptors(LoginInterceptor)
  @Post('google-login')
  async googleLogin(@Req() req: Request) {
    return await this.authService.googleLogin(req.body);
  }

  /* @UseGuards(GoogleAuthGuard)
  @Get('google')
  async googleLogin(): Promise<any> {
    return HttpStatus.OK;
  }

  @UseGuards(GoogleAuthGuard)
  @UseInterceptors(LoginInterceptor)
  @Get('google/redirect')
  async googleLoginRedirect(@Request() req: Request): Promise<any> {
    return await this.authService.googleLogin(req);
  } */
}
