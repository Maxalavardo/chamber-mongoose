import { applyDecorators, SetMetadata, UseGuards } from '@nestjs/common';
import { ApiBearerAuth } from '@nestjs/swagger';
import { RoleUserEnum } from 'src/user/enum/role-user.enum';
import { RolesGuard } from 'src/user/guards/role.guard';
import { JwtAuthGuard } from './guards/jwt-auth.guard';

export const ROLES_KEY = 'roles';

/**
 * Decorador que permite verificar la autenticacion segun un rol
 *
 * @constructor
 */
export function Auth(...roles: RoleUserEnum[]) {
  return applyDecorators(
    SetMetadata(ROLES_KEY, roles),
    ApiBearerAuth(),
    UseGuards(JwtAuthGuard, RolesGuard),
  );
}

/**
 * Decorador que permite verificar la autenticacion para todos los roles
 *
 * @constructor
 */
export function AuthAll() {
  const roles = [RoleUserEnum.ADMIN, RoleUserEnum.CLIENT];
  return applyDecorators(
    SetMetadata(ROLES_KEY, roles),
    ApiBearerAuth(),
    UseGuards(JwtAuthGuard, RolesGuard),
  );
}
