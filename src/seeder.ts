import { MongooseModule } from '@nestjs/mongoose';
import { seeder } from 'nestjs-seeder';
import { CategorySchema } from './categories/schema/category.schema';
import { LanguajeSchema } from './languajes/schema/languaje.schema';
import MongooseConfig from './libs/mongooseconfig';
import { CategoryTagSeeder } from './seeders/categoriesTags.seeder';
import { LanguajesSeeder } from './seeders/languajes.seeder';
import { TagSchema } from './tags/schema/tag.schema';
import { UserSeeder } from './seeders/user.seeder';
import { UserSchema } from './user/schema/user.schema';
import { ProfileSchema } from './user/schema/profile.shema';
import { FileSchema } from './files/schema/file.schema';
import { EventSchema } from './events/schema/event.schema';
import { GroupSchema } from './groups/schema/group.schema';
import { RollbackSeeder } from './seeders/rollback.seeder';

seeder({
  imports: [
    MongooseConfig,
    MongooseModule.forFeature([
      { name: 'user', schema: UserSchema },
      { name: 'profile', schema: ProfileSchema },
      { name: 'file', schema: FileSchema },
      { name: 'languaje', schema: LanguajeSchema },
      { name: 'category', schema: CategorySchema },
      { name: 'tag', schema: TagSchema },
      { name: 'event', schema: EventSchema },
      { name: 'group', schema: GroupSchema },
    ]),
  ],
  providers: [],
}).run([RollbackSeeder, UserSeeder, LanguajesSeeder, CategoryTagSeeder]);
