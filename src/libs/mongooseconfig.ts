import { MongooseModule } from '@nestjs/mongoose';
import 'dotenv/config';

const MongooseConfig = MongooseModule.forRoot(process.env.MONGODB_NAME, {
  user: process.env.MONGODB_USER,
  pass: process.env.MONGODB_PASSWORD,
  authSource: process.env.MONGODB_DATASOURCE,
});

export default MongooseConfig;
