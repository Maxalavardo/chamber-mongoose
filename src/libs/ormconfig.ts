import { TypeOrmModule } from '@nestjs/typeorm';

const OrmConfig = TypeOrmModule.forRoot({
  type: 'postgres',
  host: process.env.DB_HOST,
  port: parseInt(process.env.DB_PORT),
  username: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_DATABASE,
  synchronize: true,
  entities: ['dist/**/*.entity{.ts,.js}'],
});

export default OrmConfig;
