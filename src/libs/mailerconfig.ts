import { createTransport } from 'nodemailer';

export const transporter = createTransport({
  host: `${process.env.SMTP_HOST}`,
  port: 465,
  secure: true, // true for 465, false for other ports
  auth: {
    user: process.env.SMTP_USERNAME, // generated ethereal user
    pass: process.env.SMTP_PASSWORD, // generated ethereal password
  },
});
