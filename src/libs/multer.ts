import { diskStorage } from 'multer';
import * as path from 'path';
import { FilesInterceptor } from '@nestjs/platform-express';
import { BadRequestException } from '@nestjs/common';

export const Storage = FilesInterceptor('files', 10, {
  // storage
  storage: diskStorage({
    destination: './public/uploads',
    filename: function (req, file, cb) {
      const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1e9);
      cb(
        null,
        uniqueSuffix + path.extname(file.originalname).toLocaleLowerCase(),
      );
    },
  }),
  // limits
  limits: {
    fileSize: 20000000, // 20mb to bytes
    files: 8, // limit
  },
  // filters
  fileFilter: function (req, file, cb) {
    const fileType = /jpg|jpeg|png|gif/;
    const mimetype = fileType.test(file.mimetype);
    const extname = fileType.test(path.extname(file.originalname));
    if (mimetype && extname) {
      return cb(null, true);
    }
    cb(new BadRequestException('The file must be an image'), false);
  },
});
