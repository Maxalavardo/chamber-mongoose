import { PartialType } from '@nestjs/swagger';
import {
  ArrayUnique,
  IsArray,
  IsEmail,
  IsOptional,
  IsString,
  MinLength,
} from 'class-validator';
import { File } from 'src/files/schema/file.schema';
import { Tag } from 'src/tags/schema/tag.schema';
import { CreateUserDto } from './create-user.dto';

export class UpdateUserDto extends PartialType(CreateUserDto) {
  @IsOptional()
  @IsEmail()
  email: string;

  @IsOptional()
  @IsString()
  @MinLength(8)
  password: string;

  @IsOptional()
  @IsString()
  @MinLength(8)
  confirmed_password: string;

  @IsOptional()
  @IsString()
  @MinLength(4)
  username: string;

  @ArrayUnique()
  @IsArray()
  @IsOptional()
  removeTag: Tag[];

  @ArrayUnique()
  @IsArray()
  @IsOptional()
  removeFile: File[];
}
