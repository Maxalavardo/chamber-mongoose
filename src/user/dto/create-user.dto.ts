import { Transform } from 'class-transformer';
import {
  ArrayUnique,
  IsArray,
  IsBoolean,
  IsDateString,
  IsEmail,
  IsEnum,
  IsOptional,
  IsString,
  Matches,
  MinLength,
} from 'class-validator';
import { Tag } from 'src/tags/schema/tag.schema';
import { GenderUserEnum } from '../enum/gender_user.enum';

export class CreateUserDto {
  @Transform(({ value }) => String(value), { toClassOnly: true })
  @IsEmail()
  email: string;

  @Transform(({ value }) => String(value), { toClassOnly: true })
  @IsString()
  @MinLength(8)
  password: string;

  @Transform(({ value }) => String(value), { toClassOnly: true })
  @IsString()
  @MinLength(8)
  confirmed_password: string;

  @Transform(({ value }) => String(value), { toClassOnly: true })
  @IsString()
  @MinLength(4)
  username: string;

  @Transform(({ value }) => String(value), { toClassOnly: true })
  @IsString()
  @Matches(/^[A-Za-z\u00C0-\u017F\s\-]+$/, {
    message: 'name must be only letters',
  })
  first_name: string;

  @Transform(({ value }) => String(value), { toClassOnly: true })
  @IsString()
  @Matches(/^[A-Za-z\u00C0-\u017F\s\-]+$/, {
    message: 'name must be only letters',
  })
  last_name: string;

  @Transform(({ value }) => String(value), { toClassOnly: true })
  @IsString()
  @IsOptional()
  @Matches(/^[-+\][0-9\ ]+$/, {
    message: 'phone must be a valid phone number',
  })
  phone: string;

  @Transform(({ value }) => String(value), { toClassOnly: true })
  @IsDateString()
  birthdate: Date;

  @Transform(({ value }) => String(value), { toClassOnly: true })
  @IsEnum([GenderUserEnum.MALE, GenderUserEnum.FEMALE])
  gender: GenderUserEnum;

  @Transform(({ value }) => String(value), { toClassOnly: true })
  @IsEnum(GenderUserEnum)
  @IsOptional()
  sex_preference: GenderUserEnum;

  @Transform(({ value }) => Boolean(value), { toClassOnly: true })
  @IsOptional()
  @IsBoolean()
  search_friends: boolean;

  @Transform(({ value }) => Boolean(value), { toClassOnly: true })
  @IsOptional()
  @IsBoolean()
  search_crush: boolean;

  @Transform(
    ({ value }) => (Array.isArray(value) == false ? Array(value) : value),
    {
      toClassOnly: true,
    },
  )
  @IsArray()
  @ArrayUnique()
  @IsOptional()
  tag: Tag[];
}
