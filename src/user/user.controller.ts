import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
  Query,
  UseInterceptors,
  UploadedFiles,
} from '@nestjs/common';
import { UserService } from './user.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { Auth, AuthAll } from 'src/auth/auth.decorator';
import { RoleUserEnum } from './enum/role-user.enum';
import { Storage } from 'src/libs/multer';

@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post()
  @UseInterceptors(Storage)
  async create(
    @Body() createUserDto: CreateUserDto,
    @UploadedFiles() files: Array<Express.Multer.File>,
  ) {
    return await this.userService.create(createUserDto, files);
  }

  @Auth(RoleUserEnum.ADMIN)
  @Get()
  async findAll(@Query() query, @Query('pagination') pagination: boolean) {
    const { tag, page, limit } = query;
    return await this.userService.findAll(pagination, tag, page, limit);
  }

  @Auth(RoleUserEnum.ADMIN)
  @Get(':id')
  async findOne(@Param('id') id: string) {
    return await this.userService.findOne(id);
  }

  @AuthAll()
  @Put(':id')
  async update(@Param('id') id: string, @Body() updateUserDto: UpdateUserDto) {
    return await this.userService.update(id, updateUserDto);
  }

  @AuthAll()
  @Delete(':id')
  async remove(@Param('id') id: string) {
    return await this.userService.remove(id);
  }
}
