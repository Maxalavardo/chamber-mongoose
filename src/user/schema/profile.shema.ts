import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { GenderUserEnum } from '../enum/gender_user.enum';
import { User } from './user.schema';
import { File } from '../../files/schema/file.schema';
import * as paginate from 'mongoose-paginate-v2';
import { Tag } from 'src/tags/schema/tag.schema';

export type ProfileDocument = Profile & Document;

@Schema({
  timestamps: true,
  toJSON: {
    virtuals: true,
  },
})
export class Profile {
  @Prop({ required: true, unique: true, lowercase: true, type: String })
  username: string;

  @Prop({ type: String, required: true })
  first_name: string;

  @Prop({ type: String, required: true })
  last_name: string;

  @Prop({ type: String, default: null })
  phone: string;

  @Prop({ type: Date, required: true })
  birthdate: Date;

  @Prop({
    type: String,
    enum: [GenderUserEnum.MALE, GenderUserEnum.FEMALE],
  })
  gender: GenderUserEnum;

  @Prop({ type: String, enum: GenderUserEnum })
  sex_preference: GenderUserEnum;

  @Prop({ type: Boolean })
  search_friends: boolean;

  @Prop({ type: Boolean })
  search_crush: boolean;

  @Prop({ type: Types.ObjectId, ref: 'User', required: true })
  user: User;

  @Prop({ type: [{ type: Types.ObjectId, ref: 'Tag' }] })
  tag: Tag[];

  @Prop({ type: [{ type: Types.ObjectId, ref: 'File' }] })
  file: File[];

  /* @Prop({ type: Types.ObjectId, ref: 'City', required: true })
  city: City; */

  /* @Prop({ type: Types.ObjectId, ref: 'University' })
  university: University; */
}

const ProfileSchema = SchemaFactory.createForClass(Profile);
ProfileSchema.virtual('fullName').get(function (this: ProfileDocument) {
  return `${this.first_name} ${this.last_name}`;
});
ProfileSchema.plugin(paginate);

export { ProfileSchema };
