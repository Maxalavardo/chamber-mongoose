import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { RoleUserEnum } from '../enum/role-user.enum';

export type UserDocument = User & Document;

@Schema({ timestamps: true })
export class User {
  @Prop({ required: true, unique: true, lowercase: true, type: String })
  email: string;

  @Prop({ required: true, type: String })
  password: string;

  @Prop({ required: true, type: String })
  confirmed_password: string;

  @Prop({ type: String, enum: RoleUserEnum, default: RoleUserEnum.CLIENT })
  role: RoleUserEnum;

  // google id login
  @Prop({ type: String })
  googleID: string;

  @Prop({ type: Boolean })
  googleStatus: boolean;

  @Prop({ type: Date })
  createdAt?: Date;

  @Prop({ type: Date })
  updatedAt?: Date;
}

export const UserSchema = SchemaFactory.createForClass(User);
