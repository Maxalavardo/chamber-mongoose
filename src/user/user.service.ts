import {
  BadRequestException,
  ConflictException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectConnection, InjectModel } from '@nestjs/mongoose';
import { Model, Connection, PaginateModel } from 'mongoose';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { UserDocument } from './schema/user.schema';
import * as bcrypt from 'bcrypt';
import { ProfileDocument } from './schema/profile.shema';
import { RoleUserEnum } from './enum/role-user.enum';
import { FileDocument } from 'src/files/schema/file.schema';
import { TagDocument } from 'src/tags/schema/tag.schema';
import { CategoryDocument } from 'src/categories/schema/category.schema';
import { unlinkSync } from 'fs';

@Injectable()
export class UserService {
  constructor(
    @InjectModel('user')
    private readonly userModel: Model<UserDocument>,
    @InjectModel('profile')
    private readonly profileModel: Model<ProfileDocument>,
    @InjectModel('profile')
    private readonly profileModelPag: PaginateModel<ProfileDocument>,
    @InjectModel('file')
    private readonly fileModel: Model<FileDocument>,
    @InjectModel('tag')
    private readonly tagModel: Model<TagDocument>,
    @InjectModel('category')
    private readonly categoryModel: Model<CategoryDocument>,
    @InjectConnection()
    private readonly connection: Connection,
  ) {}

  async create(createUserDto: CreateUserDto, images?: any) {
    const transaction = await this.connection.startSession();
    transaction.startTransaction();
    let user = null;
    const profileImages = [],
      tagsItems = [];

    try {
      user = await this.findUser(createUserDto.email);

      if (user) throw new ConflictException('email already exists');

      if (createUserDto.password != createUserDto.confirmed_password) {
        throw new ConflictException('the password does not match');
      }

      createUserDto.password = await this.setHashPassword(
        createUserDto.password,
      );

      createUserDto.confirmed_password = await this.setHashPassword(
        createUserDto.confirmed_password,
      );

      const username = await this.profileModel.findOne({
        username: createUserDto.username.toLowerCase(),
      });

      if (username) throw new ConflictException('username in use');

      if (createUserDto.search_friends == true) {
        createUserDto.search_crush = false;
      } else if (createUserDto.search_crush == true) {
        createUserDto.search_friends = false;
      }

      if (images.length > 8) {
        throw new BadRequestException('image limit exceeded');
      }

      for await (const image of images) {
        const img = new this.fileModel();
        img.route = image.path;
        await img.save();
        profileImages.push(img);
      }

      createUserDto.tag ? createUserDto.tag : (createUserDto.tag = []);

      for await (const tag of createUserDto.tag) {
        const item = await this.tagModel.findById(tag);
        if (!item) throw new BadRequestException('tag not found');
        tagsItems.push(item._id);
      }

      user = await this.userModel.create({
        email: createUserDto.email,
        password: createUserDto.password,
        confirmed_password: createUserDto.confirmed_password,
      });

      await this.profileModel.create({
        username: createUserDto.username.toLowerCase(),
        first_name: createUserDto.first_name,
        last_name: createUserDto.last_name,
        phone: createUserDto.phone,
        birthdate: createUserDto.birthdate,
        gender: createUserDto.gender,
        sex_preference: createUserDto.sex_preference,
        search_friends: createUserDto.search_friends,
        search_crush: createUserDto.search_crush,
        user: user._id,
        tag: tagsItems,
        file: profileImages,
      });

      user = await this.profileModel.findOne({ user: user._id }).populate([
        {
          path: 'user',
          model: this.userModel,
          select: '-password -confirmed_password',
        },
        {
          path: 'file',
          model: this.fileModel,
        },
        {
          path: 'tag',
          model: this.tagModel,
          populate: { path: 'category', model: this.categoryModel },
        },
      ]);

      await transaction.commitTransaction();
    } catch (error) {
      await transaction.abortTransaction();
      return error;
    } finally {
      await transaction.endSession();
    }

    return user;
  }

  async findAll(
    pagination?: boolean,
    tag?: string,
    page?: number,
    limit?: number,
  ) {
    const filter = new Object();

    if (tag) {
      filter['tag'] = { $in: tag };
    }

    if (pagination) {
      return await this.profileModelPag.paginate(filter, {
        page: page ? page : 1,
        limit: limit ? limit : 10,
        populate: [
          {
            path: 'user',
            model: this.userModel,
            select: '-password -confirmed_password',
          },
          {
            path: 'file',
            model: this.fileModel,
          },
          {
            path: 'tag',
            model: this.tagModel,
            populate: { path: 'category', model: this.categoryModel },
          },
        ],
        sort: { _id: -1 },
      });
    }
    return await this.profileModel
      .find(filter)
      .sort({ _id: -1 })
      .populate([
        {
          path: 'user',
          model: this.userModel,
          select: '-password -confirmed_password',
        },
        {
          path: 'file',
          model: this.fileModel,
        },
        {
          path: 'tag',
          model: this.tagModel,
          populate: { path: 'category', model: this.categoryModel },
        },
      ]);
  }

  async findOne(id: string) {
    const user = await this.profileModel.findById(id).populate([
      {
        path: 'user',
        model: this.userModel,
        select: '-password -confirmed_password',
      },
      {
        path: 'file',
        model: this.fileModel,
      },
      {
        path: 'tag',
        model: this.tagModel,
        populate: { path: 'category', model: this.categoryModel },
      },
    ]);

    if (!user) throw new NotFoundException('User not found');

    return user;
  }

  async findUser(email: string): Promise<UserDocument> {
    return await this.userModel.findOne({ email });
  }

  async update(
    _id: string,
    updateUserDto: UpdateUserDto,
  ): Promise<ProfileDocument> {
    try {
      const updateProfile = await this.findOne(_id);

      // to google users
      if (updateProfile.user.googleID) {
        updateProfile.user.googleStatus = true;
      }

      if (updateUserDto.search_friends == true) {
        updateUserDto.search_crush = false;
      } else if (updateUserDto.search_crush == true) {
        updateUserDto.search_friends = false;
      }

      // addToSet and pullAll methods cannot coexist in the same update method
      await updateProfile.updateOne(
        {
          username: updateUserDto.username,
          first_name: updateUserDto.first_name,
          last_name: updateUserDto.last_name,
          phone: updateUserDto.phone,
          birthdate: updateUserDto.birthdate,
          gender: updateUserDto.gender,
          sex_preference: updateUserDto.sex_preference,
          search_friends: updateUserDto.search_friends,
          search_crush: updateUserDto.search_crush,
          $addToSet: { tag: { $each: updateUserDto.tag } },
        },
        { new: true },
      );

      if (updateUserDto.removeFile.length >= 1) {
        for await (const image of updateUserDto.removeFile) {
          const img = await this.fileModel.findById(image);

          if (!img) throw new ConflictException('Image not fount');

          unlinkSync(img.route);
          await img.deleteOne();
        }
      }

      await updateProfile.updateOne(
        {
          $pullAll: {
            tag: updateUserDto.removeTag,
            file: updateUserDto.removeFile,
          },
        },
        { new: true },
      );

      return await this.findOne(_id);
    } catch (error) {
      return error;
    }
  }

  async remove(_id: string): Promise<ProfileDocument> {
    try {
      const user = await this.findOne(_id);

      const profile = await this.profileModel.findOneAndDelete({
        _id: user._id,
      });

      if (profile.file.length >= 1) {
        for await (const image of profile.file) {
          const img = await this.fileModel.findById(image);
          unlinkSync(img.route);
          await img.deleteOne();
        }
      }

      await this.userModel.deleteOne({
        _id: profile.user,
      });

      return profile;
    } catch (error) {
      return error;
    }
  }

  async findProfileUser(user: UserDocument): Promise<UserDocument> {
    if (user.role == RoleUserEnum.ADMIN) {
      return await this.userModel
        .findOne({ _id: user._id })
        .select('-password -confirmed_password');
    } else {
      return await this.profileModel.findOne({ user: user._id }).populate([
        {
          path: 'user',
          model: this.userModel,
          select: '-password -confirmed_password',
        },
        {
          path: 'file',
          model: this.fileModel,
        },
        {
          path: 'tag',
          model: this.tagModel,
          populate: { path: 'category', model: this.categoryModel },
        },
      ]);
    }
  }

  async validateUser(id: string) {
    const user = await this.userModel.findById(id);

    if (!user) throw new NotFoundException('User not found');

    return user;
  }

  async setHashPassword(password) {
    const salt = await bcrypt.genSalt();
    return await bcrypt.hash(password, salt);
  }

  generateRandomText(lenght = 16): string {
    const characters =
      'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-';
    let text = '';

    for (let i = 0; i < lenght; i++) {
      text += characters.charAt(Math.floor(Math.random() * characters.length));
    }
    return text;
  }
}
