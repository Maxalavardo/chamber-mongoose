export enum GenderUserEnum {
  MALE = 'male',
  FEMALE = 'female',
  BOTH = 'both',
}
