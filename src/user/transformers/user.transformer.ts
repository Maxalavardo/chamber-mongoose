import { UserDocument } from '../schema/user.schema';

export class UserTransformer {
  public static transform(user: UserDocument) {
    return {
      id: user._id,
      email: user.email,
      role: user.role,
      googleStatus:
        user.googleStatus == false || true ? user.googleStatus : null,
      created_at: user.createdAt,
      updated_at: user.updatedAt,
    };
  }
}
