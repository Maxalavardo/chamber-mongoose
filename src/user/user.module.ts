import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { UserSchema } from './schema/user.schema';
import { ProfileSchema } from './schema/profile.shema';
import { FileSchema } from 'src/files/schema/file.schema';
import { TagSchema } from 'src/tags/schema/tag.schema';
import { CategorySchema } from 'src/categories/schema/category.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'user', schema: UserSchema },
      { name: 'profile', schema: ProfileSchema },
      { name: 'file', schema: FileSchema },
      { name: 'category', schema: CategorySchema },
      { name: 'tag', schema: TagSchema },
    ]),
  ],
  controllers: [UserController],
  providers: [UserService],
  exports: [UserService],
})
export class UserModule {}
