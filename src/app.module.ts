import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import MongooseConfig from './libs/mongooseconfig';
import { ExampleModule } from './example/example.module';
import { SeedersModule } from './seeders/seeders.module';
import { UserModule } from './user/user.module';
import { AuthModule } from './auth/auth.module';
import { CategoryModule } from './categories/category.module';
import { TagModule } from './tags/tag.module';
import { LanguajeModule } from './languajes/languaje.module';
import { EventsModule } from './events/events.module';
import { GroupsModule } from './groups/groups.module';
import { FilesModule } from './files/files.module';
import { MailModule } from './mail/mail.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    MongooseConfig,
    SeedersModule,
    ExampleModule,
    UserModule,
    AuthModule,
    CategoryModule,
    TagModule,
    LanguajeModule,
    EventsModule,
    GroupsModule,
    FilesModule,
    MailModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
