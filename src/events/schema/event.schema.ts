import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { Category } from 'src/categories/schema/category.schema';
import { Tag } from 'src/tags/schema/tag.schema';
import { GenderUserEnum } from 'src/user/enum/gender_user.enum';
import { TypeEventGroup } from '../enum/type_event_group.enum';
import * as paginate from 'mongoose-paginate-v2';
import { File } from 'src/files/schema/file.schema';

export type EventDocument = Event & Document;

@Schema({ timestamps: true })
export class Event {
  @Prop({ type: String, required: true })
  name: string;

  @Prop({ type: Date, required: true })
  date: Date;

  @Prop({ type: Types.ObjectId, ref: 'Category', required: true })
  theme: Category;

  @Prop({ type: String, enum: TypeEventGroup, required: true })
  type: TypeEventGroup;

  @Prop({ type: Date })
  reminder: Date;

  @Prop({ type: String, required: true })
  location: string;

  @Prop({ type: Number, required: true })
  maximum_attendance: number;

  @Prop({ type: String, required: true })
  description: string;

  @Prop({ type: String, required: true })
  ubication: string;

  @Prop({ type: [{ type: Types.ObjectId, ref: 'Tag' }] })
  nationality: Tag[];

  @Prop({ type: String, enum: GenderUserEnum })
  gender: GenderUserEnum;

  @Prop({ type: Number })
  ageInitial: number;

  @Prop({ type: Number })
  ageFinal: number;

  @Prop({ type: [{ type: Types.ObjectId, ref: 'File' }] })
  file: File[];
}

export const EventSchema = SchemaFactory.createForClass(Event);
EventSchema.plugin(paginate);
