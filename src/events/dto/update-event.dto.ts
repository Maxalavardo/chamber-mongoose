import { PartialType } from '@nestjs/swagger';
import { ArrayUnique, IsArray, IsOptional } from 'class-validator';
import { File } from 'src/files/schema/file.schema';
import { Tag } from 'src/tags/schema/tag.schema';
import { CreateEventDto } from './create-event.dto';

export class UpdateEventDto extends PartialType(CreateEventDto) {
  @ArrayUnique()
  @IsArray()
  @IsOptional()
  removeNationality: Tag[];

  @ArrayUnique()
  @IsArray()
  @IsOptional()
  removeFile: File[];
}
