import {
  BadRequestException,
  ConflictException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { unlinkSync } from 'fs';
import { Model, PaginateModel } from 'mongoose';
import { CategoryDocument } from 'src/categories/schema/category.schema';
import { FileDocument } from 'src/files/schema/file.schema';
import { TagDocument } from 'src/tags/schema/tag.schema';
import { CreateEventDto } from './dto/create-event.dto';
import { UpdateEventDto } from './dto/update-event.dto';
import { EventDocument } from './schema/event.schema';

@Injectable()
export class EventsService {
  constructor(
    @InjectModel('event')
    private readonly eventModel: Model<EventDocument>,
    @InjectModel('event')
    private readonly eventModelPag: PaginateModel<EventDocument>,
    @InjectModel('category')
    private readonly categoryModel: Model<CategoryDocument>,
    @InjectModel('tag')
    private readonly tagModel: Model<TagDocument>,
    @InjectModel('file')
    private readonly fileModel: Model<FileDocument>,
  ) {}

  async create(
    createEventDto: CreateEventDto,
    images?: Array<Express.Multer.File>,
  ) {
    const eventImages = [];
    let event = null;

    try {
      if (createEventDto.ageInitial && !createEventDto.ageFinal) {
        throw new BadRequestException('final age needed');
      } else if (!createEventDto.ageInitial && createEventDto.ageFinal) {
        throw new BadRequestException('initial age needed');
      }

      const theme = await this.categoryModel.findById(createEventDto.theme);

      if (!theme) throw new BadRequestException('Theme not found');

      for await (const image of images) {
        const img = new this.fileModel();
        img.route = image.path;
        await img.save();
        eventImages.push(img);
      }

      event = await this.eventModel.create({
        name: createEventDto.name,
        date: createEventDto.date,
        theme: theme._id,
        type: createEventDto.type,
        reminder: createEventDto.reminder,
        location: createEventDto.location,
        maximum_attendance: createEventDto.maximum_attendance,
        description: createEventDto.description,
        ubication: createEventDto.ubication,
        nationality: createEventDto.nationality,
        gender: createEventDto.gender,
        ageInitial: createEventDto.ageInitial,
        ageFinal: createEventDto.ageFinal,
        file: eventImages,
      });
    } catch (error) {
      throw new BadRequestException(error);
    }

    return await this.eventModel
      .findById(event._id)
      .populate({
        path: 'theme',
        model: this.categoryModel,
      })
      .populate({
        path: 'nationality',
        model: this.tagModel,
        populate: {
          path: 'category',
          model: this.categoryModel,
        },
      })
      .populate({
        path: 'file',
        model: this.fileModel,
      });
  }

  async findAll(pagination?: boolean, page?: number, limit?: number) {
    if (pagination) {
      return await this.eventModelPag.paginate(
        {},
        {
          page: page ? page : 1,
          limit: limit ? limit : 10,
          populate: [
            {
              path: 'theme',
              model: this.categoryModel,
            },
            {
              path: 'nationality',
              model: this.tagModel,
              populate: {
                path: 'category',
                model: this.categoryModel,
              },
            },
            {
              path: 'file',
              model: this.fileModel,
            },
          ],
          sort: { _id: -1 },
        },
      );
    }
    return await this.eventModel
      .find()
      .sort({ _id: -1 })
      .populate({
        path: 'theme',
        model: this.categoryModel,
      })
      .populate({
        path: 'nationality',
        model: this.tagModel,
        populate: {
          path: 'category',
          model: this.categoryModel,
        },
      })
      .populate({
        path: 'file',
        model: this.fileModel,
      });
  }

  async findOne(id: string) {
    const event = await this.eventModel
      .findById(id)
      .populate({
        path: 'theme',
        model: this.categoryModel,
      })
      .populate({
        path: 'nationality',
        model: this.tagModel,
        populate: {
          path: 'category',
          model: this.categoryModel,
        },
      })
      .populate({
        path: 'file',
        model: this.fileModel,
      });

    if (!event) throw new NotFoundException('Event not found');

    return event;
  }

  async update(id: string, updateEventDto: UpdateEventDto) {
    const updateEvent = await this.findOne(id);

    await updateEvent.updateOne(
      {
        name: updateEventDto.name,
        date: updateEventDto.date,
        theme: updateEventDto.theme,
        type: updateEventDto.type,
        reminder: updateEventDto.reminder,
        location: updateEventDto.location,
        maximum_attendance: updateEventDto.maximum_attendance,
        description: updateEventDto.description,
        ubication: updateEventDto.ubication,
        gender: updateEventDto.gender,
        ageInitial: updateEventDto.ageInitial,
        ageFinal: updateEventDto.ageFinal,
        $addToSet: { nationality: { $each: updateEventDto.nationality } },
      },
      { new: true },
    );

    if (updateEventDto.removeFile.length >= 1) {
      for await (const image of updateEventDto.removeFile) {
        const img = await this.fileModel.findById(image);

        if (!img) throw new ConflictException('Image not fount');

        unlinkSync(img.route);
        await img.deleteOne();
      }
    }

    await updateEvent.updateOne(
      {
        $pullAll: {
          nationality: updateEventDto.removeNationality,
          file: updateEventDto.removeFile,
        },
      },
      { new: true },
    );

    return await this.findOne(id);
  }

  async remove(id: string) {
    const event = await this.findOne(id);

    for await (const img of event.file) {
      unlinkSync(img.route);
      await this.fileModel.findOneAndRemove({ route: img.route });
    }

    return await this.eventModel
      .findByIdAndDelete(id)
      .populate({
        path: 'theme',
        model: this.categoryModel,
      })
      .populate({
        path: 'nationality',
        model: this.tagModel,
        populate: {
          path: 'category',
          model: this.categoryModel,
        },
      })
      .populate({
        path: 'file',
        model: this.fileModel,
      });
  }
}
