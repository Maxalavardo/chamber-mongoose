import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
  Query,
  UseInterceptors,
  UploadedFiles,
} from '@nestjs/common';
import { EventsService } from './events.service';
import { CreateEventDto } from './dto/create-event.dto';
import { UpdateEventDto } from './dto/update-event.dto';
import { AuthAll } from 'src/auth/auth.decorator';
import { Storage } from 'src/libs/multer';

@Controller('events')
export class EventsController {
  constructor(private readonly eventsService: EventsService) {}

  @AuthAll()
  @UseInterceptors(Storage)
  @Post()
  async create(
    @Body() createEventDto: CreateEventDto,
    @UploadedFiles() files: Array<Express.Multer.File>,
  ) {
    return await this.eventsService.create(createEventDto, files);
  }

  @AuthAll()
  @Get()
  async findAll(@Query() query, @Query('pagination') pagination: boolean) {
    const { page, limit } = query;
    return await this.eventsService.findAll(pagination, page, limit);
  }

  @AuthAll()
  @Get(':id')
  async findOne(@Param('id') id: string) {
    return await this.eventsService.findOne(id);
  }

  @AuthAll()
  @Put(':id')
  async update(
    @Param('id') id: string,
    @Body() updateEventDto: UpdateEventDto,
  ) {
    return await this.eventsService.update(id, updateEventDto);
  }

  @AuthAll()
  @Delete(':id')
  async remove(@Param('id') id: string) {
    return await this.eventsService.remove(id);
  }
}
