import { Module } from '@nestjs/common';
import { EventsService } from './events.service';
import { EventsController } from './events.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { EventSchema } from './schema/event.schema';
import { CategorySchema } from 'src/categories/schema/category.schema';
import { TagSchema } from 'src/tags/schema/tag.schema';
import { FileSchema } from 'src/files/schema/file.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'event', schema: EventSchema },
      { name: 'category', schema: CategorySchema },
      { name: 'tag', schema: TagSchema },
      { name: 'file', schema: FileSchema },
    ]),
  ],
  controllers: [EventsController],
  providers: [EventsService],
})
export class EventsModule {}
